package com.example.ud8_ejemplo1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import com.example.ud8_ejemplo1.databinding.ActivityInsertarTrabajadorBinding

class InsertarTrabajador : AppCompatActivity() {
    private lateinit var binding: ActivityInsertarTrabajadorBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityInsertarTrabajadorBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        val nombre = binding.nombreEdit

        binding.insertarBoton.setOnClickListener {
            intent = Intent()

            // Si el editText está vacío, cancelamos.
            if(TextUtils.isEmpty(nombre.text.toString()))
                setResult(RESULT_CANCELED, intent)
            else {
                intent.putExtra("trabajador", nombre.text.toString())

                setResult(RESULT_OK, intent)
            }

            finish()
        }
    }
}