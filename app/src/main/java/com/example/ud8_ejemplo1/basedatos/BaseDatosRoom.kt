package com.example.ud8_ejemplo1.basedatos

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import java.util.concurrent.Executors

// Definimos la base de datos con todas las entidades creadas (en nuestro caso una), la versión
// y no exportamos el "schema".
@Database(entities = [Trabajador::class], version = 1, exportSchema = false)
abstract class BaseDatosRoom : RoomDatabase(){
    abstract fun daoTrabajador() : DaoTrabajador

    companion object {
        // Instancia de la Base de Datos.
        // La declaramos como @Volatile para asegurarnos que su lectura y escritura sea desde
        // la memoria principal y no desde la caché.
        @Volatile
        private var INSTANCIA : BaseDatosRoom? = null

        // Definimos 4 hilos.
        private const val NUM_HILOS = 4

        //Creamos un atributo de tipo "ExecutorService" con el número de hilos definidos antes (4)
        // para realizar operaciones asíncronas en la base de datos sobre el hilo "background".
        val databaseWriterExecutor = Executors.newFixedThreadPool(NUM_HILOS)

        // Método para crear/acceder a la base de datos (basedatos_trabajador).
        fun obtenerBaseDatos (context: Context) : BaseDatosRoom {
                synchronized(this) {
                    var instancia = INSTANCIA

                    if(instancia == null) {
                        instancia = Room.databaseBuilder(context.applicationContext,
                            BaseDatosRoom::class.java, "basedatos_trabajador")
                            //.fallbackToDestructiveMigration() // Añadimos la estrategia de migración
                            .build()

                        INSTANCIA = instancia
                    }
                    return instancia
                }
        }
    }
}