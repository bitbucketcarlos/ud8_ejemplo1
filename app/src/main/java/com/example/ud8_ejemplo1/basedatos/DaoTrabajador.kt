package com.example.ud8_ejemplo1.basedatos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

// Dao con 3 operaciones SQL: obtener todos los trabajadores ordenados alfabéticamente, insertar
// un trabajador y actualizar el nombre de uno ya insertado.
@Dao
interface DaoTrabajador {
    @Query("SELECT * FROM tabla_trabajador ORDER BY nombre ASC")
    fun obtenerTrabajadoresOrdenados() : LiveData<List<Trabajador>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertar(trabajador: Trabajador)

    @Query("UPDATE tabla_trabajador SET nombre =:nombre WHERE id =:id")
    fun actualizar(id: Int, nombre: String)
}