package com.example.ud8_ejemplo1.basedatos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

// Nombre de la tabla: tabla_trabajador
// Campos: id (Clave primaria, autogenerado),
//         nombre (no null).
@Entity(tableName = "tabla_trabajador")
data class Trabajador(@NonNull @ColumnInfo(name = "nombre") var nombre: String?){
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0
}