package com.example.ud8_ejemplo1

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ud8_ejemplo1.basedatos.Trabajador
import com.example.ud8_ejemplo1.databinding.ActivityMainBinding
import com.example.ud8_ejemplo1.viewmodel.TrabajadorViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var trabajadorViewModel: TrabajadorViewModel

    val resultInsertar = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val datosResult = result.data
            val trabajador = Trabajador(datosResult?.getStringExtra("trabajador"))

            trabajadorViewModel.insertar(trabajador)
        }
    }

    val resultActualizar = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val datosResult = result.data

            datosResult?.getIntExtra("id", 0)
                ?.let { datosResult.getStringExtra("nombre")
                    ?.let { it1 -> trabajadorViewModel.actualizar(it, it1) } }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        val recycler = binding.recyclerview

        recycler.setHasFixedSize(true)
        recycler.addItemDecoration(DividerItemDecoration(this, 1))
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        val trabajadorAdapter = TrabajadorAdapter()

        trabajadorAdapter.setOnItemClickListerner {
            val posicionPulsada = recycler.getChildAdapterPosition(it)
            val trabajador = trabajadorAdapter.getLista()[posicionPulsada]
            val intent = Intent(this, ActualizarTrabajador::class.java)

            intent.putExtra("id", trabajador.id)
            intent.putExtra("nombre", trabajador.nombre)

            resultActualizar.launch(intent)
        }

        recycler.adapter = trabajadorAdapter

        // Obtenemos uno nuevo o ya existente ViewModel desde la clase ViewModelProviders.
        trabajadorViewModel = ViewModelProvider(this).get(TrabajadorViewModel::class.java)

        // Añadimos un observador al LiveData devuelto por el método obtenerTodosTrabajadores.
        // el método onchanged se ejecuta cuando el observador detecta un cambio y la actividad está
        // en segundo plano.
        trabajadorViewModel.obtenerTodosTrabajadores().observe(this) { trabajador ->
            // Actualizamos el cambio en el RecyclerView.
            trabajadorAdapter.anyadirALista(trabajador as ArrayList<Trabajador>)
        }

        // Buscamos el FAB y configuramos su onClickListener
        binding.fab.setOnClickListener {
            val intent = Intent(this, InsertarTrabajador::class.java)

            resultInsertar.launch(intent)
        }
    }
}