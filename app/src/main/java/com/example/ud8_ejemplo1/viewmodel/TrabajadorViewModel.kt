package com.example.ud8_ejemplo1.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.ud8_ejemplo1.basedatos.Trabajador
import com.example.ud8_ejemplo1.repositorio.RepositorioTrabajador

class TrabajadorViewModel(aplicacion: Application) : AndroidViewModel(aplicacion) {
    // Los atributos de la clase serán uno de tipo RespositorioTrabajador para trabajar con la base de
    // datos a través de él y una lista de tipo LiveData con listas de trabajadores.
    private var repositorioTrabajador: RepositorioTrabajador
    private var todosTrabajadores: LiveData<List<Trabajador>>

    init {
        repositorioTrabajador = RepositorioTrabajador(aplicacion)

        // Obtenemos todos los trabajadores del repositorio.
        todosTrabajadores = repositorioTrabajador.obtenerTodosTrabajadores()
    }

    // Método para obtener todos los trabajadores.
    fun obtenerTodosTrabajadores () : LiveData<List<Trabajador>> {
        return todosTrabajadores
    }

    // Método para insertar un trabajador a través del repositorio.
    fun insertar(trabajador: Trabajador) {
        repositorioTrabajador.insertar(trabajador)
    }

    // Método para actualizar el nombre de un trabajador a través del repositorio.
    fun actualizar(id: Int, nombre: String) {
        repositorioTrabajador.actualizar(id, nombre)
    }
}