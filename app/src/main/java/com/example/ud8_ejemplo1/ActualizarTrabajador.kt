package com.example.ud8_ejemplo1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import com.example.ud8_ejemplo1.databinding.ActivityActualizarTrabajadorBinding

class ActualizarTrabajador : AppCompatActivity() {
    private lateinit var binding: ActivityActualizarTrabajadorBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityActualizarTrabajadorBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        val nombre = binding.nombreEditAct
        nombre.setText(intent.getStringExtra("nombre"))

        val id = intent.getIntExtra("id", 0)


        binding.actualizarBoton.setOnClickListener {
            intent = Intent()

            // Si el editText está vacío, cancelamos.
            if(TextUtils.isEmpty(nombre.text.toString()))
                setResult(RESULT_CANCELED, intent)
            else {
                intent.putExtra("nombre", nombre.text.toString())
                intent.putExtra("id", id)

                setResult(RESULT_OK, intent)
            }

            finish()
        }
    }
}